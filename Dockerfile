# build environment
FROM node:16.15.1-alpine 
WORKDIR /app
COPY package.json ./
RUN npm install
COPY . .

# production environment build
RUN npm run build -p

